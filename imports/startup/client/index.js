import Vue from 'vue'
import {Meteor} from 'meteor/meteor'
import { ApolloClient } from "apollo-client";
import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import VueApollo from 'vue-apollo'
import VueMeteorTracker from 'vue-meteor-tracker'

const httpLink = new HttpLink({
    uri: Meteor.absoluteUrl('graphql')
});

const cache = new InMemoryCache();

const client = new ApolloClient({
    link: httpLink,
    cache: cache,
    connectToDevTools: true,
});

const apolloProvider = new VueApollo({
    defaultClient: client,
  })


// Install the vue plugin
Vue.use(VueApollo)
Vue.use(VueMeteorTracker)

export default apolloProvider