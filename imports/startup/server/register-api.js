import { createApolloServer } from "meteor/apollo";
import { makeExecutableSchema } from "graphql-tools";

import ResolutionsShema from '../../api/resolutions/Resolutions.graphql'
import ResolutionsResolvers from '../../api/resolutions/resolvers'
// asfa
// Here we define what the Queries will return. resolutions() will return an Array of Resolutions which we importet in ResolutionsShema
const testSchema = `
type Query {
    resolutions: [Resolution]
}`

const typeDefs = [
    testSchema, 
    ResolutionsShema
]

const resolvers = {
    Query: {
        ...ResolutionsResolvers.Query
    },
    Mutation: {
        ...ResolutionsResolvers.Mutation
    }
}

const schema = makeExecutableSchema({
    typeDefs,
    resolvers
})

createApolloServer({schema});