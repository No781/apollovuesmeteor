import {Meteor} from 'meteor/meteor';
import Vue from 'vue';
import apolloProvider from '/imports/startup/client/index.js';

// Main app
import App from '/imports/ui/App.vue';

Meteor.startup(() => {
  new Vue({
    el: '#app',
    provide: apolloProvider.provide(),
    connectToDevTools: true,
    render: h => h(App),
  })
});